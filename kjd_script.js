/**GLOBAL VARIABLES**/
var guest_list = [];
var xmlDoc;
/**LOAD GUEST LIST FROM XML**/
var xhttp = new XMLHttpRequest
xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        myFunction(this);
    }
};
xhttp.open("GET", "guestlist.xml", true);
xhttp.send();

function myFunction(xml) {

    xmlDoc = xml.responseXML;
    var xml_guest_list = xmlDoc.childNodes[0];
    // Retrieve each guest
    for (var i = 0; i < xml_guest_list.children.length; i++) {
        var guest = xml_guest_list.children[i];
        // Access each of the data values.
        var new_guest = {
            "Id": i,
            "Firstname": guest.getElementsByTagName("first_name")[0].textContent.toString(), "Lastname": guest.getElementsByTagName("last_name")[0].textContent.toString(),
            "Company": guest.getElementsByTagName("company")[0].textContent.toString()
        };
        guest_list.push(new_guest);
    }
    //call function that will display guest list
    loadGuestlist(guest_list);
}

/**SEARCH GUEST LIST**/
document.getElementById("cmd_search").addEventListener("click", function () {
    var search_result_list = [];
    var txt_search = document.getElementById("input_search").value.toUpperCase();
    var guest_found = [];
    if (txt_search.length > 0) {
        for (var i = 0; i < guest_list.length; i++) {
            if (guest_list[i].Firstname.toUpperCase() === txt_search || guest_list[i].Lastname.toUpperCase() === txt_search || guest_list[i].Company.toUpperCase() === txt_search) {
                guest_found.push(guest_list[i]);
            }
        }
        loadGuestlist(guest_found, true);
        //remove message if visiblea
        document.getElementById("msg_bar").classList.add("hide");
    }
    else
        displayMessage("Enter a guests name", true);
});
/*
 * Function that displays the contents of a list of guestlist objects 
 * @guestlist list/array of guestlist object
 * @search is the origin of call from search function
*/
function loadGuestlist(guestlist, search) {
    var txt_html = "";
    for (var i = 0; i < guestlist.length; i++) {
        // store data in text to be added to page
        txt_html += "<li id='" + guestlist[i].Id + "' class='select_guest'><span></span> <p>" + guestlist[i].Firstname + " " + guestlist[i].Lastname + "</p> <label class='hide'>" + guestlist[i].Company + "</label>";
        if (guest_list[i].Mobile != null && guestlist[i].Email != null) {
            txt_html += "<label class='mobile'>" + guestlist[i].Mobile + ", " + guestlist[i].Email + "</label>";
        }
        txt_html += "</li>";
    }
    document.getElementById("ul_guest_list").innerHTML = txt_html;
    //customize message
    if (!search)
        document.getElementById("num_of_guests").innerHTML = guestlist.length + " Confirmed guests";
    else
        document.getElementById("num_of_guests").innerHTML = guestlist.length + " Guests Found";

    /*add guest selection listener*/
    var select_guests = document.getElementsByClassName("select_guest");
    for (var k = 0; k < select_guests.length; k++) {
        select_guests[k].addEventListener('click', selectFunction, false);
    }
}
/**Function that handles a guest being selected**/
function selectFunction() {
    var slected_guest = this.getAttribute("Id");
    //hide&show different sections
    document.getElementById("guestlist_search_holder").classList.add("hide");
    document.getElementById("guest_details_holder").classList.remove("hide");
    document.getElementById("guest_details_holder").classList.add("show");
    //load guest data into card
    var selected_guest = guest_list[slected_guest];
    document.getElementById("guest_fullname").innerHTML = selected_guest.Firstname + " " + selected_guest.Lastname;
    document.getElementById("guest_company").innerHTML = selected_guest.Company;
    document.getElementById("guest_id").val = selected_guest.Id;
    //remove message if visiblea
    document.getElementById("msg_bar").classList.add("hide");
}
/**ADD DETAILS**/
document.getElementById("add_details").addEventListener("click", function () {
    var mobile = document.getElementById("new_mobile").value;
    var email = document.getElementById("new_email").value;
    if (mobile.length) {
        if (validateNumber(mobile)) {
            if (email.length) {
                var guest_index = document.getElementById("guest_id").val;
                //update xml
                newMobile = xmlDoc.createElement("mobile");
                newMobileText = xmlDoc.createTextNode(mobile);
                newMobile.appendChild(newMobileText);
                var element = xmlDoc.getElementsByTagName("record")[guest_index];
                element.appendChild(newMobile);
                //update object
                guest_list[guest_index]["Mobile"] = mobile;
                guest_list[guest_index]["Email"] = email;
                //display list
                document.getElementById("guest_details_holder").classList.add("hide");
                document.getElementById("guestlist_search_holder").classList.remove("hide");
                document.getElementById("guestlist_search_holder").classList.add("show");
                loadGuestlist(guest_list);
                //remove message if visible
                document.getElementById("msg_bar").classList.add("hide");
            }
            else
                displayMessage("Add email address", true);
        }
        else
            displayMessage("Add a valid cellphone number", true);
    }
    else
        displayMessage("Add mobile number", true);
});

/*
 * displays message to user
 * @txt_message text of message
 * @warning determines colour of message displayed
*/
function displayMessage(txt_message, warning) {
    if (warning)
        document.getElementById("msg_bar").classList.add("warning");
    document.getElementById("msg_bar").classList.remove("hide");
    document.getElementById("txt_msg").innerText = txt_message;
}

/*
 * Function that validates cellphone number
 * @cellphone cell hpone number to be validated 
 */
function validateNumber(txt_cell) {
    var validator = txt_cell.replace(/\D/g, '').length;//most sa numbers are 10 digits long or 11 when starting with dialing code(+27)
    if (validator === 10 || validator === 11)
        return true;
    else
        return false;
} 